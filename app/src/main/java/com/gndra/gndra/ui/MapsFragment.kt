package com.gndra.gndra.ui

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.gndra.gndra.R
import com.gndra.gndra.data.ApiServiceInfoMaps
import com.gndra.gndra.domain.MapsUseCase
import com.gndra.gndra.models.InfoZipCode
import com.gndra.gndra.viewmodel.MapsActivityViewModel
import com.gndra.gndra.viewmodel.MapsActivityViewModelFactory
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.fragment_maps.*


class MapsFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnPolylineClickListener,
    GoogleMap.OnPolygonClickListener{
    private lateinit var mMap: GoogleMap

    private lateinit var viewModel:MapsActivityViewModel

    var polygon:Polygon?= null
    var polyOpt:PolygonOptions?=null

    var lat:ArrayList<LatLng>?= arrayListOf<LatLng>(
        LatLng(-34.747, 145.592),
        LatLng(-34.364, 147.891),
        LatLng(-33.501, 150.217),
        LatLng(-32.306, 149.248),
        LatLng(-32.491, 147.309)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v=inflater.inflate(R.layout.fragment_maps, container, false)
        val mapFragment = this.childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        return v!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupButtons()
    }

    fun setupViewModel(){
        viewModel= ViewModelProviders.of(this, MapsActivityViewModelFactory(MapsUseCase())).get(
            MapsActivityViewModel::class.java)
        viewModel.attachView(this)
    }

    fun setupButtons(){
        lateinit var service: ApiServiceInfoMaps
        var poly:com.gndra.gndra.models.PolygonMaps?=null
        btn_Buscar.setOnClickListener {
            viewModel.getPolygon("04600")
        }
        text_CP.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
            override fun afterTextChanged(s: Editable?) {
                if(s!!.length===5){
                    viewModel.getPolygon(s.toString())
                    hideKeyboard()

                }else if(s!!.length>5){
                    Toast.makeText(activity,"el codigo postal solo debe llevar 5 digitos", Toast.LENGTH_SHORT).show()
                }
                else if(s!!.length===0){
                    cleanText()
                }
                tv_counter.text="${s!!.length} / 5 "
            }
        })

    }
    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }
    fun Activity.hideKeyboard() {
        if (currentFocus == null) View(this) else currentFocus?.let { hideKeyboard(it) }
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showException(message:String){
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    }
    override fun onDetach() {
        super.onDetach()
        viewModel.dettachJob()
        viewModel.dettachView()
    }



    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
    }
    fun setInfoViews(info:InfoZipCode){
        text_Pais.setText("México")
        text_Entity.setText(info.locality)
        text_City.setText(info.federalEntity.name)
        text_Municipality.setText(info.municipality.name)
        text_Suburb.setText(info.settlements.get(0).name)
    }
    fun cleanText(){
        text_Pais.setText("")
        text_Entity.setText("")
        text_City.setText("")
        text_Municipality.setText("")
        text_Suburb.setText("")

    }

    fun drawPoligon(listCoordinates:ArrayList<LatLng>){
    var buildL=LatLngBounds.builder()
// Constrain the camera target to the Adelaide bounds.
        polyOpt= PolygonOptions()
            .clickable(true)

        for (la in listCoordinates!!){
            polyOpt!!.add(la)
            buildL.include(la!!)

        }
        var center=buildL.build()
        val polyline1:Polygon = mMap.addPolygon(
            polyOpt!!
        )
        polyline1.fillColor=Color.BLUE
        val centro = LatLng(center.center.latitude, center.center.longitude)
        mMap.addMarker(MarkerOptions().position(centro).title("Zona"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(buildL.build(),0 ))
    }
    fun showProgressBar(){
        activity!!.progressBar.visibility=View.VISIBLE
        activity!!.getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }
    fun hideProgressBar(){
        activity!!.progressBar.visibility=View.GONE
        activity!!.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }



    override fun onPolylineClick(p0: Polyline?) {

    }

    override fun onPolygonClick(p0: Polygon?) {

    }

}