package com.gndra.gndra.data

import com.gndra.gndra.models.InfoZipCode
import com.gndra.gndra.models.PolygonMaps
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiServiceInfoMaps {
    @GET("zip-codes/{query}")
    fun getZipCodes( @Path("query")query : String): retrofit2.Call<PolygonMaps>

    @GET("/api/zip-codes/{query}")
    fun getInfoZipCodes( @Path("query")query : String): retrofit2.Call<InfoZipCode>
}