package com.gndra.gndra.data

import com.gndra.gndra.exception.Exceptions
import com.gndra.gndra.models.InfoZipCode
import com.gndra.gndra.models.PolygonMaps
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class InfoMapsDataSet {
    lateinit var service:ApiServiceInfoMaps
    var poly:com.gndra.gndra.models.PolygonMaps?=null
    var info:InfoZipCode?=null
    var listCoordinates:ArrayList<LatLng>?= arrayListOf()

    suspend fun searchsPolygon(zip_code:String):Unit= suspendCancellableCoroutine{
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://poligonos-wje6f4jeia-uc.a.run.app")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create<ApiServiceInfoMaps>(ApiServiceInfoMaps::class.java)

        service.getZipCodes(zip_code).enqueue(object : Callback<PolygonMaps> {
            override fun onResponse(
                call: Call<PolygonMaps>,
                response: retrofit2.Response<PolygonMaps>
            ) {
                poly=response.body()
                if(poly == null){
                    it.resumeWithException(Exceptions("No se encontraron los datos, rebice el código Postal o intentenlo más tarde"))
                }
                else{
                    listCoordinates=listLatlng(poly!!.geometry!!.coordinates.get(0))
                    it.resume(Unit)
                }

            }
            override fun onFailure(call: Call<PolygonMaps>, t: Throwable) {
                it.resumeWithException(Exceptions(t.message.toString()))
            }
        })

    }
    suspend fun searchsInfoZipCode(zip_code:String):Unit= suspendCancellableCoroutine{
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://sepomex-wje6f4jeia-uc.a.run.app")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create<ApiServiceInfoMaps>(ApiServiceInfoMaps::class.java)

        service.getInfoZipCodes(zip_code).enqueue(object : Callback<InfoZipCode> {
            override fun onResponse(
                call: Call<InfoZipCode>,
                response: retrofit2.Response<InfoZipCode>
            ) {
                info=response.body()
                if(info == null){
                    it.resumeWithException(Exceptions("No se encontraron los datos, rebice el código Postal o intentenlo más tarde"))
                }
                else{
                    it.resume(Unit)
                }

            }
            override fun onFailure(call: Call<InfoZipCode>, t: Throwable) {
                it.resumeWithException(Exceptions(t.message.toString()))
            }
        })

    }
    fun listLatlng(list: List<List<Double>>):ArrayList<LatLng>{
        var coordinates:ArrayList<LatLng>?= arrayListOf()

        for (coor in list){
            coordinates!!.add(
                LatLng(coor.get(1),coor.get(0))
            )
        }
        //Log.e("retrofit","coor =  tam = ${coordinates!!}")
        return coordinates!!
    }

    fun getListcoordinates():ArrayList<LatLng>{
        return  this.listCoordinates!!
    }
    fun getInfoZipCode():InfoZipCode{
        return  this.info!!
    }


}