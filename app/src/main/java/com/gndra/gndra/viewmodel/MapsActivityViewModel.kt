package com.gndra.gndra.viewmodel

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModel
import com.gndra.gndra.MapsActivity
import com.gndra.gndra.domain.MapsUseCase
import com.gndra.gndra.models.InfoZipCode
import com.gndra.gndra.ui.MapsFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class MapsActivityViewModel(val mapsUseCase: MapsUseCase):ViewModel(),CoroutineScope {
    val job=Job()
    private var view:MapsFragment?=null
    var listCoordinates:ArrayList<LatLng>?= arrayListOf()
    var info: InfoZipCode?=null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main+job

    fun getPolygon(zip_code: String){
        launch{
            try{
                view?.showProgressBar()
                mapsUseCase.searchsPolygon(zip_code)
                listCoordinates=mapsUseCase.getCoordinates()
                view?.drawPoligon(listCoordinates!!)
                mapsUseCase.searchsInfoZipCode(zip_code)
                info=mapsUseCase.getInfoZipCode()
                view?.setInfoViews(info!!)
                view?.hideProgressBar()
            }catch (e: Exception){
                 view?.showException(e.message.toString())
                 view?.hideProgressBar()
            }

        }


    }


    fun attachView(view:MapsFragment){
        this.view=view
    }
    fun dettachView(){
        this.view=null
    }

    fun dettachJob(){
        coroutineContext.cancel()
    }


}