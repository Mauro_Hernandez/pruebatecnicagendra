package com.gndra.gndra.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gndra.gndra.domain.MapsUseCase

class MapsActivityViewModelFactory(val mapsUseCase: MapsUseCase):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(MapsUseCase::class.java).newInstance(mapsUseCase)
    }
}