package com.gndra.gndra.models


import com.google.gson.annotations.SerializedName

data class InfoZipCode(
    @SerializedName("zip_code")
    var zipCode: String,
    var locality: String,
    @SerializedName("federal_entity")
    var federalEntity: FederalEntity,
    var settlements: List<Settlement>,
    var municipality: Municipality
)