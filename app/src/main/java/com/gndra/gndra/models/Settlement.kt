package com.gndra.gndra.models


import com.google.gson.annotations.SerializedName

data class Settlement(
    var key: String,
    var name: String,
    @SerializedName("zone_type")
    var zoneType: String,
    @SerializedName("settlement_type")
    var settlementType: SettlementType
)