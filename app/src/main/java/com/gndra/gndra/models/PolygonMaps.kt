package com.gndra.gndra.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PolygonMaps(
    var type: String?="",
    var error: String,
    var geometry: Geometry?=null
):Parcelable