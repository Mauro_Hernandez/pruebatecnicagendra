package com.gndra.gndra.models


import com.google.gson.annotations.SerializedName

data class FederalEntity(
    var key: String,
    var name: String,
    var code: String
)