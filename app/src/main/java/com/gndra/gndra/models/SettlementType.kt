package com.gndra.gndra.models


data class SettlementType(
    var key: String,
    var name: String
)