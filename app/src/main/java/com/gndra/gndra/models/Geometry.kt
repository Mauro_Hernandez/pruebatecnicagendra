package com.gndra.gndra.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Geometry(
    var type: String,
    var coordinates: List<List<List<Double>>>
):Parcelable