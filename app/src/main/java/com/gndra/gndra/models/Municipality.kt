package com.gndra.gndra.models


import com.google.gson.annotations.SerializedName

data class Municipality(
    var key: String,
    var name: String
)