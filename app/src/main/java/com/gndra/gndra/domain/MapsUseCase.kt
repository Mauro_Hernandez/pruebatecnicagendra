package com.gndra.gndra.domain

import com.gndra.gndra.data.InfoMapsDataSet
import com.gndra.gndra.models.InfoZipCode
import com.google.android.gms.maps.model.LatLng

class MapsUseCase {
    val dataSet=InfoMapsDataSet()

    suspend fun searchsPolygon(zip_code:String){
        dataSet.searchsPolygon(zip_code)
    }

    fun getCoordinates():ArrayList<LatLng>{
        return dataSet.getListcoordinates()
    }

    suspend fun searchsInfoZipCode(zip_code:String){
        dataSet.searchsInfoZipCode(zip_code)
    }

    fun getInfoZipCode():InfoZipCode{
        return dataSet.getInfoZipCode()
    }


}